<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>

		<form class="box" action="pages/login.php" method="post" name="login">
			<h1 class="box-title">Connexion</h1>
			<?php
				session_start();
				if (! empty($_SESSION['informationMessage'])) { 
				    echo "<p class=\"errorMessage\">";
				    echo $_SESSION['informationMessage'];
					echo "</p>";
					$_SESSION['informationMessage'] = "";
				} 
			    session_unset (); // On détruit les variables de notre session
			    session_destroy (); // On détruit notre session
			    session_write_close(); // On ferme la session
			?>
			<input type="text" class="box-input" name="email" placeholder="Email">
			<input type="password" class="box-input" name="password" placeholder="Mot de passe">
			<input type="submit" value="Connexion " name="submit" class="box-button">
			<p class="box-register">Vous êtes nouveau ici? <a href="pages/sign_up.php">S'inscrire</a></p>
		</form>	
    </body>
</html> 
