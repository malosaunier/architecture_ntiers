# Mobilités

## 1. Utilisation
### Connexion
Lorsque l'utilisateur accède au site web, il tombe sur la page d'accueil. Deux possibilités s'offrent à lui:
- il peut se connecter s'il est déjà enregistré.
- ou choisir de s'inscrire s'il ne l'est pas déjà en cliquant sur `S'incrire`. Dans ce cas, une fenêtre d'inscription lui sera proposée et lui permettra de retourner à l'étape de connexion lorsque celle-ci sera complète.

_A noter que l'identifiant de connexion sera l'**adresse mail** indiquée lors de l'inscription._

Suivant, les identifiants fournis lors de la connexion, l'utilisateur aura accès à l'un des espaces suivant:
- admin
- étudiant

### Accès étudiant
Dans le cas d'un accès étudiant, l'utilisateur pourra:
- visualiser l'historique de ces mobilités
- rechercher une mobilité précise
- ordonner les mobilités suivant une colonne
- ajouter un nouveau voyage
- supprimer un voyage
- modifier un voyage, seulement dans le cas où son état est _A venir_

### Accès admin
Puis, dans le cas où il s'agit d'un admin, les fonctionnalités sont les suivantes:
- visualiser l'historique des mobilités de l'ensemble des étudiants
- rechercher une mobilité précise
- ordonner les mobilités suivant une colonne
- filtrer les mobilités suivant une colonne (filtres dans le pied de tableau)
- supprimer un voyage
- modifier un voyage
- accéder à la carte des mobilités

## 2. Fonctionnement
### Accès à la base de données
A chaque fois que la base de données est requêtée le fichier `config.php` est intégré dans le code. Il comprend notamment la variable de connexion à la base. Les éléments d'authentification sont stockés sur le serveur à l'aide des variables d'environnement d'apache. Le fichier de configuration est donc accrédité des mentions : `SetEnv [VAR] [VALUE]` et les valeurs associées sont récupérées dans le fichier `config.php` à l'aide de la fonction : `getenv()`. L’objectif est d’éviter le stockage des identifiants dans les fichiers sources php.

### Connexion
La demande de connexion à l'application est réalisée avec la page `index.php`. Pour vérifier la validité des identifiants, une requête post est exécutée via le formulaire de connexion en utilisant la page `login.php`. Suivant la validité de la demande, l'utilisateur est redirigé, soit dans l'espace admin : page `admin_trip_page.php`, soit dans l'espace étudiant : page `student_page.php`. En cas d'identifiants inexistants, aucune redirection n'est réalisée et un message d'erreur est affiché.

### Inscription
L'inscription se présente sur la page `sign_up.php` et est accessible depuis la page de connexion. Lors de cette étape, une vérification de l'adresse mail est réalisée:
- validité --> en utilisant les fonctions js contenues dans le fichier `email_validation.js`
- unicité --> via le fichier js précédent donnant lieu à une requête post (ajax) en utilisant la page `unique_email_address.php`.  

Une vérification du mot de passe saisie est également réalisée (confirmation) via `valid_form.js`.  
Ainsi, dans le cas où les éléments saisis sont corrects, et que l'utilisateur lance la demande d'inscription, une requête post est exécutée via le formulaire en utilisant la page `register.php`.

### Espace étudiant
La page principale de l'espace étudiant est `student_page.php`. Sur celle-ci, l'utilisateur peut effectuer plusieurs actions détaillées dans la partie **Affichage, ajout, modification et suppression des voyages**.  
L'ensemble de ces pages utilisent le fichier js : `app_student_page.js` et le fichier de style : `app_student_page.css`.

### Espace admin
La page principale de l'espace admin est `admin_trip_page.php`. En effet, l'admin a accès à trois pages :
- la page principale (les actions sont détaillées dans la partie **Affichage, ajout, modification et suppression des voyages**).
- la page `admin_map_page.php` affichant la carte des mobilités
    - pour la map, nous nous basons sur une librairie js [amCharts](https://www.amcharts.com/) qui nous permet d'afficher une map du monde. Tout le traitement lié à cette map se trouve dans `app_admin_page.js`. Dans ce fichier nous avons une fonction `createDynamicMap()` qui prend en entrée un objet js qui contient une liste de clé-valeur des codes ISO des pays (clé) dans la BDD avec le nombre de voyage dans ce pays (valeur). Cet objet est construit à l'aide d'une requête HTTP de type GET sur `get_all_trip.php` effectuée avec du Ajax. Une fois les pays contenus dans la BDD récupérés, on les affiche en rouge sur la map. Si on passe la souris sur ces mêmes pays, on peut voir le nom du pays correspondant ainsi que le nombre de voyages dans celui-ci.
- la page `admin_config_page.php` donnant accès à des éléments de configuration  (possibilité de créer d'autres comptes admin).

L'ensemble de ces pages utilisent le fichier de style :  `app_admin_page.css`.  

### Affichage, ajout, modification et suppression des voyages
- L'ajout d'un voyage s'effectue à partir d'une requête post exécutée via le formulaire d'ajout en utilisant la page `insert_trip.php` (seulement pour l'espace étudiant). Tous les champs rentrés par l'utilisateur vont être traités et envoyés à la base de données. Pour le champ "pays", la valeur envoyée et stockée dans la BDD va être le code [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) correspondant au pays sélectionné (ex : France --> FR) car nous avons besoin de ce code ISO pour la map dans la partie admin.
- Pour la modification d'un voyage, l'application utilisera le fichier `update_trip.php` (espace étudiant et admin).
- Enfin, la suppression d'un voyage sera réalisée via le fichier `delete_trip.php` (espace étudiant et admin).  

Pour éviter la duplication de code sur ces fonctionnalités utilisées dans les deux espaces, des fichiers js et css regroupent la gestion des mobilités, respectivement : `trip_management.js` et `style_table_trip_page.css`. 

### Gestion des sessions
Quelques soient les droits accordés, la session expire au bout de 3 minutes d'inactivité sur le site, en utilisant sur chaque page la méthode `manage_session()` contenue dans `functions.php`.

### Déconnexion
Quelques soient les droits accordés, l'utilisateur peut se déconnecter de son espace. Pour cela, lors de cette demande, la page `logout.php` est appelée.


