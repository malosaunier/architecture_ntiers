<?php
  function console_log($value){
    echo "<script>console.log('" . $value . "' );</script>";
  }

  // Permet de gérer l'expiration des sessions
  function manage_session(){
  	// On restaure la session
  	session_start();

  	// Permet de savoir si une connexion a eu lieu
	if(!isset($_SESSION['uid'])){
		header ('location: ../index.php');
	}
	
	// Si la variable de session a été créée et que le temps est dépassé
	if ((isset($_SESSION['CREATED'])) && (time() - $_SESSION['CREATED'] > 900)) { //3min
		// On change le message de la page de connexion
		$_SESSION['informationMessage'] = "La session a expiré vous avez été redirigé à la page de connexion";
		session_write_close();
	    // On retourne à la page de connexion
	    header ('location: ../index.php');
	}
	// Sinon on actualise la variable (en cas d'utilisation)
	$_SESSION['CREATED'] = time();

    session_write_close();
  }
?>