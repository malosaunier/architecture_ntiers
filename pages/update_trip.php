<?php
  require('config.php');

  include('functions.php');


  // Gestion de la session
  manage_session();
  
  // Restaure la session trouvée sur le serveur
  session_start ();
  if (isset($_REQUEST['country'], $_REQUEST['town'], $_REQUEST['beginDate'], $_REQUEST['endDate'], $_REQUEST['rowIndex'])){
    
    // On traite tous les attributs
    $country = stripslashes($_REQUEST['country']);
    $country = mysqli_real_escape_string($conn, $country); 

    $town = stripslashes($_REQUEST['town']);
    $town = mysqli_real_escape_string($conn, $town); 

    $beginDate = stripcslashes($_REQUEST['beginDate']);
    $beginDate = mysqli_real_escape_string($conn, $beginDate);

    $endDate = stripslashes($_REQUEST['endDate']);
    $endDate = mysqli_real_escape_string($conn, $endDate);

    $trip_id = $_REQUEST['rowIndex'];
    

    // On crée la requête
    $query = "UPDATE trip_app.TRIP
                SET country = '$country', town = '$town', beginDate = '$beginDate', endDate = '$endDate'
                  WHERE trip_id = $trip_id";

    // On exécute la requête sur la base de données
    $res = mysqli_query($conn, $query);

    // On retourne sur la page étudiant
    header ('location: student_page.php');
  }
  session_write_close();
  
?>