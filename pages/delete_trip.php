<?php
  require('config.php');

  include('functions.php');

  // Gestion de la session
  manage_session();

  // Restaure la session trouvée sur le serveur
  session_start ();
  if (isset($_REQUEST['rowIndex'])){

    $trip_id = $_REQUEST['rowIndex'];

    // On crée la requête
    $query = "DELETE FROM trip_app.TRIP WHERE trip_id = $trip_id";

    // On exécute la requête sur la base de données
    $res = mysqli_query($conn, $query);

    // On retourne sur la page étudiant
    header ('location: student_page.php');
  }
  session_write_close();
?>