<?php
	include('functions.php');
	manage_session();
?>
<html>
	<head>
		<title>Page admin configurations</title>

		<!-- Bootstrap-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<!-- Police Montserrat -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" />

		<!-- Icones bootstrap-->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

		<!-- Custom style -->
		<link rel="stylesheet" href="../css/style_admin_page.css" />

	</head>

	<body>

		<header class="navbar sticky-top navbar-expand-lg navbar-dark flex-column flex-md-row bg-dark">
			<a class="navbar-brand" href="#">Admin</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
			    <ul class="navbar-nav mr-auto">
			      <li class="nav-item">
			      	<a class="nav-link" href="./admin_trip_page.php">Voyages</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="./admin_map_page.php">Carte</a>
			      </li>
			      <li class="nav-item active">
			        <a class="nav-link" href="">Configurations</a>
			      </li>
			    </ul>
			    <ul class="navbar-nav">
			      <li class="nav-item navbar-right">
			        <a id="headerLogout" class="nav-link" href="./logout.php">Déconnexion</a>
			      </li>
			    </ul>
		  	</div>
		</header>

		<div class="container-fluid">

			<h1 class="bd-title">Configurations</h1>

			<?php 
				if (! empty($_SESSION['inscriptionMessage'])) { 
				    echo "<p class=\"successMessage\">";
				    echo $_SESSION['inscriptionMessage'];
					echo "</p>";
					$_SESSION['inscriptionMessage'] = "";
				} 
			    session_unset (); // On détruit les variables de notre session
		    ?>
			<form class="form" action="register_admin.php" method="post">

		      <h2 class="box-title">Créer un compte admin</h1>
		      <input type="text" class="box-input" name="lastname" placeholder="Nom" required />
		      <input type="text" class="box-input" name="firstname" placeholder="Prénom" required />
		      <input type="text" class="box-input" id="email" name="email" placeholder="Email" required onkeyup='ValidateEmail();'/>

		      <input type="password" class="box-input" name="password" id ="password" placeholder="Mot de passe" required/>

		      <input type="password" class="box-input" id="confirm_password" placeholder="Confirmation" required onkeyup='check();'/>

		      <input type="submit" name="submit" value="S'inscrire" class="box-button" onclick="return checkUniqueEmail();"/>

		    </form>

		</div>

	</body>


	<script
            src="https://code.jquery.com/jquery-3.4.1.slim.js"
            integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
            crossorigin="anonymous">
            	
    </script>

	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- Custom js -->
	<script type="text/javascript" src="../js/valid_form.js"></script>
	<script type="text/javascript" src="../js/email_validation.js"></script>
</html>



