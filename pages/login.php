<?php
require('config.php');

if (isset($_REQUEST['email']) && isset($_REQUEST['password'])){
  $email = stripslashes($_REQUEST['email']);
  $email = mysqli_real_escape_string($conn, $email);
  $password = stripslashes($_REQUEST['password']);
  $password = mysqli_real_escape_string($conn, $password);
  $query = "SELECT * FROM trip_app.USER WHERE email='$email' and password='".hash('sha256', $password)."'";
  $result = mysqli_query($conn,$query) or die(mysql_error());
  $rowNumber = mysqli_num_rows($result);

  session_start();
  if($rowNumber==1){
     //On stocke les données correspondant à l'utilisateur dans les variables de session
    $row = mysqli_fetch_row($result);
    $_SESSION['uid'] = $row[0];
    $_SESSION['firstname'] = $row[1];
    $_SESSION['lastname'] = $row[2];
    $_SESSION['schoolYear'] = $row[4];
    $_SESSION['email'] = $email;

    // On vérifie le statut de l'utilisateur    
    $isAdmin = $row[3];
    if(boolval($isAdmin)){
      header("Location: admin_trip_page.php");
    }
    else{
      header("Location: student_page.php");
    }
    
    
  }
  else{
    $_SESSION['informationMessage'] = "Le nom d'utilisateur ou le mot de passe est incorrect.";
    header("Location: ../index.php");
  }

  session_cache_limiter('private');

  /* Configure le délai d'expiration à 1 minutes */
  session_cache_expire(1);

  session_write_close();
}
?>