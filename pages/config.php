<?php 
	// Connexion à la base de données MySQL 
	$conn = mysqli_connect(getenv('MYSQL_SERVER'), getenv('MYSQL_USERNAME'), getenv('MYSQL_PASSWORD'), getenv('MYSQL_NAME'));
	// Appel des variables d'environnements d'apache

	// Vérifier la connexion
	if($conn === false){
	    die("ERREUR : Impossible de se connecter. " . mysqli_connect_error());
	}
?>