<?php
	include('functions.php');
	manage_session();
?>
<html>
	<head>
		<title>Page admin map</title>

		<!-- Bootstrap-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<!-- Police Montserrat -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" />

		<!-- Custom style -->
		<link rel="stylesheet" href="../css/style_admin_page.css" />

	</head>

	<body>

		<header class="navbar sticky-top navbar-expand-lg navbar-dark flex-column flex-md-row bg-dark">
			<a class="navbar-brand" href="#">Admin</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
			    <ul class="navbar-nav mr-auto">
			      <li class="nav-item">
			      	<a class="nav-link" href="./admin_trip_page.php">Voyages</a>
			      </li>
			      <li class="nav-item active">
			        <a class="nav-link" href="">Carte</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="./admin_config_page.php">Configurations</a>
			      </li>
			    </ul>
			    <ul class="navbar-nav">
			      <li class="nav-item navbar-right">
			        <a id="headerLogout" class="nav-link" href="./logout.php">Déconnexion</a>
			      </li>
			    </ul>
		  	</div>
		</header>

		<div class="container-fluid">

			<h1 class="bd-title">Carte des mobilités</h1>

			<!-- amCharts libraries & plugins -->
			<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
			<script src="https://cdn.amcharts.com/lib/4/maps.js"></script>
			<script src="https://cdn.amcharts.com/lib/4/geodata/worldLow.js"></script>

			<div id="worldMap"></div>
	
		</div>
		</div>
	</body>

	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	

	<!-- Custom js -->
	<script src="../js/app_admin_page.js"></script>

</html>



