<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Student mobilities</title>
    <link rel="stylesheet" href="../css/style.css" />
  </head>
  <body>

    <form class="box" action="register.php" method="post">

      <h1 class="box-title">S'inscrire</h1>
      <input type="text" class="box-input" name="lastname" placeholder="Nom" required />
      <input type="text" class="box-input" name="firstname" placeholder="Prénom" required />
      <select name="promo" id="promo_select">
        <option value="FISE1">FISE1</option>
        <option value="FISE2">FISE2</option>
        <option value="FISE3">FISE3</option>
      </select>
      <input type="text" class="box-input" id="email" name="email" placeholder="Email" required onkeyup='ValidateEmail();'/>

      <input type="password" class="box-input" name="password" id ="password" placeholder="Mot de passe" required/>

      <input type="password" class="box-input" id="confirm_password" placeholder="Confirmation" required onkeyup='check();'/>

      <input type="submit" name="submit" value="S'inscrire" class="box-button" onclick="return checkUniqueEmail();"/>
      <p class="box-register">Déjà inscrit? <a href="../index.php">Connectez-vous ici</a></p>

    </form>
  </body>
  <!-- Jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

  <!-- Custom js -->
  <script type="text/javascript" src="../js/valid_form.js"></script>
  <script type="text/javascript" src="../js/email_validation.js"></script>
  
</html>