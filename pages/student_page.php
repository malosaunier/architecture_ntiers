<?php
	include('functions.php');
	manage_session();
?>
<html>
	<head>
		<title>Page étudiant</title>
		<!-- Bootstrap table -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.css"/>

		<!-- Police Montserrat -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" />

		<!-- Icones bootstrap-->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

		<!-- Custom style -->
		<link rel="stylesheet" href="../css/style_student_page.css" />
		<link rel="stylesheet" href="../css/style_table_trip_page.css" />
	</head>

	<body>

		<header class="headerBand">
			<?php
				session_start ();
				// Afficher les caractéristiques de l'utilisateur
				echo '<p>' . $_SESSION['firstname'] .' '. $_SESSION['lastname'] . ' ' . $_SESSION['schoolYear'] . '</p>';
				session_write_close();
			?>
			<button id="userConnectionButton">
				<i class="fa fa-user fa-2x" aria-hidden="true"></i>
			</button>
		</header>


		<div class="userConnectionDiv">
			<p id='profilIcon' >
				<i class="fa fa-user fa-5x" aria-hidden="true"></i>
			</p>
			<?php
				session_start ();
				// Afficher les caractéristiques de l'utilisateur
				echo '<p>'. $_SESSION['firstname'] .' '. $_SESSION['lastname'] . '</p>';
				echo '<p>
						<i class="fa fa-graduation-cap" aria-hidden="true"></i>' 
						. '   ' . $_SESSION['schoolYear'] . 
					  '</p>';
				echo '<p id="emailTextUserConnection">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						' . $_SESSION['email'] . 
					 '</p>';
				session_write_close();
			?>
			<a id="userLogout" href="./logout.php">Déconnexion</a>
		</div>

		<div class="container-fluid">

			<h1>Historique des mobilités</h1>

			<script src="../js/iso_countries.js"></script>

			<div class="table-responsive table-striped">
				<table id="tripTable" class="table table-striped table-bordered">
					<thead>
					    <tr>
					    	<th>N°</th>
					        <th>Date d'ajout</th>
					        <th>Pays</th>
					        <th>Ville</th>
					        <th>Date de début</th>
					        <th>Date de fin</th>
					        <th>Etat</th>
					        <th id="actionColumn">Actions</th>
					    </tr>
					</thead>
					<tbody>
						<?php 
		  					require('config.php');
							session_start ();
							// On récupère le user id courant
							$uid = $_SESSION['uid'];
							// On crée la requête
							$query = "SELECT creationDate, country, town, beginDate, endDate, trip_id FROM trip_app.TRIP WHERE user_id = '$uid'";
							// On exécute la requête sur la base de données
		  					$result = mysqli_query($conn,$query) or die(mysql_error());

		  					$_SESSION['trip_state_array'] = array();

		  					$today = strtotime(date("Y-m-d"));

		  					$tripState = "";
		  					$classStatename = "";

		  					while($row = mysqli_fetch_array($result, MYSQLI_NUM))
							{
								// Calcul des états du voyage
								$startDate = strtotime($row[3]);
								$endDate = strtotime($row[4]);

								if($today <= $startDate){
									$tripState = "A venir";
									$classStatename = "stateSoon";
								}
								elseif ($today > $endDate) {
									$tripState = "Archivée";
									$classStatename = "stateArchived";
									$buttonEditTrip = "";
								}
								else{
									$tripState = "En cours";
									$classStatename = "stateInProgress";
								}

								$countries = json_decode(file_get_contents('http://country.io/names.json'));
								$countryName=strval($row[1]);
								$row[1]=$countries->$countryName;

								// Affichage du voyage
							    echo "<tr>
							    		  <td>$row[5]</td>
									      <td>$row[0]</td>
									      <td>$row[1]</td>
									      <td>$row[2]</td>
									      <td>$row[3]</td>
									      <td>$row[4]</td>
									      <td>
									      	<p class=\"$classStatename\">$tripState</p>
									      </td>
									      <td>
									      	<div class=\"editDeleteTripButton\">";
								
								// Si le voyage n'est pas en cours ni archivé
								if($tripState != "Archivée" && $tripState != "En cours"){
									echo "<button class=\"editTripButton\">
											<i class=\"fa fa-pencil-square-o fa-lg\" aria-hidden=\"true\"></i>
										  </button>";
								}


								echo    		"<button class=\"deleteTripButton\">
													<i class=\"fa fa-trash-o fa-lg\" aria-hidden=\"true\"></i>
												</button> 
								    		</div>
				    						</td>
						    	  	  </tr>";
						    	array_push($_SESSION['trip_state_array'], $tripState);
							}

							session_write_close();
						?>
					</tbody>
				</table>
			</div>
		</div>

		<div class='bottomPageButtonBand'>
			<button id="buttonAddTrip" class="buttonTrip" onclick="addTrip()">Ajouter un voyage</button>
		</div>

		<div class="tripFormDiv">
		</div>

	</body>

	<script
            src="https://code.jquery.com/jquery-3.4.1.slim.js"
            integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
            crossorigin="anonymous">	
    </script>

	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- Bootstrap table -->
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.js"></script>

	<!-- Custom js -->
	<script src="../js/app_student_page.js"></script>
	<script src="../js/trip_management.js"></script>

</html>



