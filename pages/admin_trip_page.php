<?php
	include('functions.php');
	manage_session();
?>
<html>
	<head>
		<title>Page admin trips</title>
		<!-- Bootstrap table -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.css"/>

		<!-- Police Montserrat -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat" />

		<!-- Icones bootstrap-->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

		<!-- Custom style -->
		<link rel="stylesheet" href="../css/style_admin_page.css" />
		<link rel="stylesheet" href="../css/style_table_trip_page.css" />


	</head>

	<body>

		<header class="navbar sticky-top navbar-expand-lg navbar-dark flex-column flex-md-row bg-dark">
			<a class="navbar-brand" href="#">Admin</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
			    <ul class="navbar-nav mr-auto">
			      <li class="nav-item active">
			      	<a class="nav-link" href="">Voyages</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="./admin_map_page.php">Carte</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link" href="./admin_config_page.php">Configurations</a>
			      </li>
			    </ul>
			    <ul class="navbar-nav">
			      <li class="nav-item navbar-right">
			        <a id="headerLogout" class="nav-link" href="./logout.php">Déconnexion</a>
			      </li>
			    </ul>
		  	</div>
		</header>

		<div class="container-fluid">

			<h1 class="bd-title">Historique des mobilités</h1>

			<div class="table-responsive table-striped">
				<table id="tripTable" class="table table-striped table-bordered">
					<thead>
					    <tr>
					    	<th>N°</th>
					        <th>Date d'ajout</th>
					        <th>Pays</th>
					        <th>Ville</th>
					        <th>Date de début</th>
					        <th>Date de fin</th>
					        <th>Nom</th>
					        <th>Prénom</th>
					        <th>Filière</th>
					        <th>Etat</th>
					        <th id="actionColumn">Actions</th>
					    </tr>
					</thead>
					<tbody>
						<?php 
		  					require('config.php');
							session_start ();
							// On crée la requête
							$query = "SELECT creationDate, country, town, beginDate, endDate, trip_id, name, firstname, schoolYear FROM trip_app.TRIP INNER JOIN trip_app.USER WHERE user_id=uid";
							// On exécute la requête sur la base de données
		  					$result = mysqli_query($conn,$query) or die(mysql_error());

		  					$_SESSION['trip_state_array'] = array();

		  					$today = strtotime(date("Y-m-d"));

		  					$tripState = "";
		  					$classStatename = "";

		  					while($row = mysqli_fetch_array($result, MYSQLI_NUM))
							{
								// Calcul des états du voyage
								$startDate = strtotime($row[3]);
								$endDate = strtotime($row[4]);

								if($today <= $startDate){
									$tripState = "A venir";
									$classStatename = "stateSoon";
								}
								elseif ($today > $endDate) {
									$tripState = "Archivée";
									$classStatename = "stateArchived";
									$buttonEditTrip = "";
								}
								else{
									$tripState = "En cours";
									$classStatename = "stateInProgress";
								}

								$countries = json_decode(file_get_contents('http://country.io/names.json'));
								$countryName=strval($row[1]);
								$row[1]=$countries->$countryName;

								// Affichage du voyage
							    echo "<tr>
							    		  <td>$row[5]</td>
									      <td>$row[0]</td>
									      <td>$row[1]</td>
									      <td>$row[2]</td>
									      <td>$row[3]</td>
									      <td>$row[4]</td>
									      <td>$row[6]</td>
									      <td>$row[7]</td>
									      <td>$row[8]</td>								  
									      <td>
									      	<p class=\"$classStatename\">$tripState</p>
									      </td>
									      <td>
									      	<div class=\"editDeleteTripButton\">
									      		<button class=\"editTripButton\">
													<i class=\"fa fa-pencil-square-o fa-lg\" aria-hidden=\"true\"></i>
										  		</button>
										  		<button class=\"deleteTripButton\">
													<i class=\"fa fa-trash-o fa-lg\" aria-hidden=\"true\"></i>
												</button> 
								    		</div>
				    					  </td>
						    	  	  </tr>";
						    	array_push($_SESSION['trip_state_array'], $tripState);
							}

							session_write_close();
						?>
					</tbody>
					<tfoot>
					    <tr>
					    	<th>N°</th>
					        <th>Date d'ajout</th>
					        <th>Pays</th>
					        <th>Ville</th>
					        <th>Date de début</th>
					        <th>Date de fin</th>
					        <th>Nom</th>
					        <th>Prénom</th>
					        <th>Filière</th>
					        <th>Etat</th>
					        <th>Actions</th>
					    </tr>
					</tfoot>
				</table>
			</div>
		</div>

	</body>

	<script
            src="https://code.jquery.com/jquery-3.4.1.slim.js"
            integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
            crossorigin="anonymous">
    </script>
	<!-- Jquery -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- Bootstrap table -->
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.23/datatables.min.js"></script>

	<!-- Custom js -->
	<script src="../js/trip_management.js"></script>
</html>



