<?php
  require('config.php');

  include('functions.php');

  // Gestion de la session
  manage_session();

  // Restaure la session trouvée sur le serveur
  session_start ();
  // On crée la requête
  $countries = "SELECT country FROM trip_app.TRIP";

  $result = mysqli_query($conn,$countries) or die(mysql_error());

  $data = array();
  while($enr = mysqli_fetch_assoc($result)){
    $a = array($enr['country']);
    array_push($data, $a);
  }

  echo json_encode($data);

  session_write_close();
?>