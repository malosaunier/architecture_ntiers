<?php
  require('config.php');

  // Gestion de la session
  session_start();
  
  // Restaure la session trouvée sur le serveur
  if (isset($_REQUEST['email'])) {
    

    $return_arr = array();

    // On traite tous les attributs
    $email = stripslashes($_REQUEST['email']);
    $email = mysqli_real_escape_string($conn, $email); 
    

    // On crée la requête
    $query = "SELECT uid FROM trip_app.USER WHERE email = '" . $email . "'";

    // On exécute la requête sur la base de données
    $res = mysqli_query($conn, $query);

    // On remplie le tableau avec les uid des users correpondant à l'adresse mail
    while($row = mysqli_fetch_array($res)){
      $return_arr[] = $row[0];
    }

    // Encoding array in JSON format
    echo json_encode($return_arr);
  }

  session_write_close();
?>