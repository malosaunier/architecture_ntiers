<?php
  require('config.php');

  include('functions.php');

  // Gestion de la session
  manage_session();

  // Restaure la session trouvée sur le serveur
  session_start ();
  if (isset($_REQUEST['email'], $_REQUEST['password'])){

    $lastname = stripslashes($_REQUEST['lastname']);
    $lastname = mysqli_real_escape_string($conn, $lastname); 

    $firstname = stripslashes($_REQUEST['firstname']);
    $firstname = mysqli_real_escape_string($conn, $firstname); 

    $email = stripslashes($_REQUEST['email']);
    $email = mysqli_real_escape_string($conn, $email);

    $password = stripslashes($_REQUEST['password']);
    $password = mysqli_real_escape_string($conn, $password);
    //requéte SQL + mot de passe crypté
    $query = "INSERT into trip_app.USER (firstname, name, admin, schoolYear, email, password)
                VALUES ('$firstname', '$lastname', '1','admin', '$email', '".hash('sha256', $password)."')";
    // Exécuter la requête sur la base de données
    $res = mysqli_query($conn,$query);
  }

  $_SESSION['inscriptionMessage'] = "Le compte admin a été créé avec succès !";

  session_write_close();

  // On retourne sur la même page
  header ('location: admin_config_page.php');
?>