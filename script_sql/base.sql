# Création base de données
CREATE DATABASE IF NOT EXISTS `trip_app`;

# Création table User
CREATE TABLE `USER` (
  `uid` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `admin` tinyint NOT NULL DEFAULT '0',
  `schoolYear` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

# Création table Trip
CREATE TABLE `TRIP` (
  `trip_id` int NOT NULL AUTO_INCREMENT,
  `country` varchar(50) NOT NULL,
  `town` varchar(50) NOT NULL,
  `beginDate` date NOT NULL,
  `endDate` date NOT NULL,
  `creationDate` datetime NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`trip_id`),
  KEY `fk_user_id_idx` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `USER` (`uid`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;