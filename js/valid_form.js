let password = document.getElementById("password");
let confirm_password = document.getElementById("confirm_password");
var check = function() {
  if (password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Password don't match");
    confirm_password.reportValidity();
  } else {
    confirm_password.setCustomValidity("");
    confirm_password.reportValidity();
	}
}