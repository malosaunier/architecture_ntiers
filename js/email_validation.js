// Valide si l'adresse mail est correcte (synthaxe)
function ValidateEmail() {
	let email = document.getElementById("email");
	var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	if(email.value.match(mailformat)) {
		email.setCustomValidity("");
		email.reportValidity();
		email.focus();
		return true;
	}
	else {
		email.setCustomValidity("Invalid email address !");
		email.reportValidity();
		email.focus();
		return false;
	}
}


// Valide si l'adresse mail n'existe pas déjà
function checkUniqueEmail(){
	let email = document.getElementById("email");
	let occurence=1;

	// On récupère le nombre d'occurences correspondant à cet email
	$.ajax({
			url: 'unique_email_address.php',
		    type: 'GET',
		    async: false,			    
		    data : 'email=' + email.value,
		    dataType: 'JSON',
		    success: function(response) {
		        occurence = response.length;
		    }
		});

	// On traite l'inscription suivant le nombre d'occurences
	if (occurence === 0){
		return true;
	}else{
		email.setCustomValidity("Email already used !");
		email.reportValidity();
		email.focus();
		return false;
	}
}