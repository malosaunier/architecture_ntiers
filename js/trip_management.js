// Numéro de la ligne sélectionnée
var rowIndex;
// L'objet html correspondant à la ligne sélectionnée
var rowObject;
// Etat des formulaires dans l'application
var isTripAddFormDisplayed = false;
// Objet pour stocker les propriétés d'un voyage (avant modification)
var updateTrip = new Object();
// Nom de la page trip admin
var adminPageTripName = "admin_trip_page.php"


// Bootstrap, gestion de la table
$(document).ready(function() {
	// S'il s'agit de la page admin
	let url = $(location).attr('href');
	if (url.substring(url.lastIndexOf('/') + 1) === adminPageTripName){
		// Ajout des inputs de filtrage dans chaque case du tableau
	    $('#tripTable tfoot th').not(":last").each(function () {
	        let title = $(this).text();
	        $(this).html( '<input type="text" placeholder="' + title + '"style="width:' + $(this).width() + '" />' );
	    });

	    $('#tripTable tfoot th:last').html("");
	 
	    // DataTable
	    let table = $('#tripTable').DataTable({
	        initComplete: function () {
	            // Apply the search
	            this.api().columns().every(function () {
	                let that = this;
	 
	                $('input', this.footer() ).on('keyup change clear', function () {
	                    if (that.search() !== this.value) {
	                        that
	                            .search(this.value)
	                            .draw();
	                    }
	                });
	            });
	        }
	    });
	}else{
		$('#tripTable').DataTable();
	}

 
});


// Permet d'ajouter un voyage dans l'application
function addTrip() {
	// Si le formulaire de modification est affiché ou que celui d'ajout ne l'est pas
	if(isTripAddFormDisplayed === false){

		// On désctive les boutons de suppression et de modification durant la phase d'ajout
		$('.editTripButton').attr("disabled", true);
		$('.deleteTripButton').attr("disabled", true);

		// On modifie le texte du bouton
		$('#buttonAddTrip').html("<i class=\"fa fa-times fa-lg\" aria-hidden=\"true\"></i>");

		let today = new Date();

		// On affiche le formulaire d'ajout de voyage
		$(".tripFormDiv").append(
			'<form action="insert_trip.php" onSubmit="return validateAddtrip()" method="post"> \
				<p>Ajouter voyage</p> \
				<select name="country" id="countryAddInput"><option value="">--- Sélectionner un pays ---</option><option value="AF">Afghanistan</option><option value="AX">Åland Islands</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia, Plurinational State of</option><option value="BQ">Bonaire, Sint Eustatius and Saba</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo</option><option value="CD">Congo, the Democratic Republic of the</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="CI">Côte d\'Ivoire</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CW">Curaçao</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and McDonald Islands</option><option value="VA">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran, Islamic Republic of</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KP">Korea, Democratic People\'s Republic of</option><option value="KR">Korea, Republic of</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Lao People\'s Democratic Republic</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao</option><option value="MK">Macedonia, the former Yugoslav Republic of</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia, Federated States of</option><option value="MD">Moldova, Republic of</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory, Occupied</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Réunion</option><option value="RO">Romania</option><option value="RU">Russian Federation</option><option value="RW">Rwanda</option><option value="BL">Saint Barthélemy</option><option value="SH">Saint Helena, Ascension and Tristan da Cunha</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="MF">Saint Martin (French part)</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SX">Sint Maarten (Dutch part)</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="SS">South Sudan</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syrian Arab Republic</option><option value="TW">Taiwan, Province of China</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania, United Republic of</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="US">United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela, Bolivarian Republic of</option><option value="VN">Viet Nam</option><option value="VG">Virgin Islands, British</option><option value="VI">Virgin Islands, U.S.</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select> \
				<input type="text" id="townAddInput" name="town" placeholder="Ville" required /> \
				<input type="date" id="beginDateAddInput" name="beginDate" required /> \
				<input type="date" id="endDateAddInput" name="endDate" required /> \
				<button type="submit" name="submit">Ajouter</button> \
			</form>');
		$(".tripFormDiv").css('border', '2px solid #2f3664');
		isTripAddFormDisplayed = true;
	}
	else{
		// On enlève la bordure de la div
		$(".tripFormDiv").css('border', '0px solid #2f3664');

		// On vide la div
		$(".tripFormDiv").empty();

		// On rétablie l'activation des boutons
		$('.editTripButton').attr("disabled", false);
		$('.deleteTripButton').attr("disabled", false);

		// On modifie le texte du bouton
		$('#buttonAddTrip').html("Ajouter un voyage");

		isTripAddFormDisplayed = false;

	}
}


// Suppression d'un voyage
$('.deleteTripButton').click(function(){
	rowObject = $(this).parent().parent().parent();
	rowIndex = $(rowObject.children()[0]).text();

	// Demande de confirmation pour suppression du voyage
	let response = confirm("Vous allez supprimer le voyage n°" + rowIndex + "\nVoulez-vous continuer ?");
	// Création de la requête post
	if (response === true) {
		$.ajax({
			url: 'delete_trip.php',
		    type: 'POST',			    
		    data : 'rowIndex=' + rowIndex,
		    success: function() {   
		        location.reload();  
		    }
		});	
	} 			
});


// Modification d'un voyage
$('.editTripButton').click(function(){
	// On désactive les autres boutons
	$('.editTripButton').attr("disabled", true);
	$('.deleteTripButton').attr("disabled", true);
	$('#buttonAddTrip').attr("disabled", true);

	// On récupère l'objet html de la ligne correspondante
	rowObject = $(this).parent().parent().parent();
	rowIndex = $(rowObject.children()[0]).text();

	// Récupération des données actuelles du voyage
	updateTrip['country'] = $(rowObject.children()[2]).text();
	updateTrip['town'] = $(rowObject.children()[3]).text();
	updateTrip['beginDate'] = $(rowObject.children()[4]).text();
	updateTrip['endDate'] = $(rowObject.children()[5]).text();


	// On remplie les cases avec des inputs initialisés avec les valeurs actuelles
	$(rowObject.children()[2]).html('<select name="country" id="countryUpdateInput" style="width:' + $(rowObject.children()[2]).width() + '" px><option value="">--- Sélectionner un pays ---</option><option value="AF">Afghanistan</option><option value="AX">Åland Islands</option><option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia, Plurinational State of</option><option value="BQ">Bonaire, Sint Eustatius and Saba</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo</option><option value="CD">Congo, the Democratic Republic of the</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="CI">Côte d\'Ivoire</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CW">Curaçao</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GG">Guernsey</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and McDonald Islands</option><option value="VA">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran, Islamic Republic of</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IM">Isle of Man</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JE">Jersey</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KP">Korea, Democratic People\'s Republic of</option><option value="KR">Korea, Republic of</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Lao People\'s Democratic Republic</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao</option><option value="MK">Macedonia, the former Yugoslav Republic of</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia, Federated States of</option><option value="MD">Moldova, Republic of</option><option value="MC">Monaco</option><option value="MN">Mongolia</option><option value="ME">Montenegro</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory, Occupied</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Réunion</option><option value="RO">Romania</option><option value="RU">Russian Federation</option><option value="RW">Rwanda</option><option value="BL">Saint Barthélemy</option><option value="SH">Saint Helena, Ascension and Tristan da Cunha</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="MF">Saint Martin (French part)</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SX">Sint Maarten (Dutch part)</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="SS">South Sudan</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syrian Arab Republic</option><option value="TW">Taiwan, Province of China</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania, United Republic of</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="US">United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela, Bolivarian Republic of</option><option value="VN">Viet Nam</option><option value="VG">Virgin Islands, British</option><option value="VI">Virgin Islands, U.S.</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option></select>');
	$("#countryUpdateInput option:contains(" + updateTrip['country'] + ")").attr('selected', 'selected');
	$(rowObject.children()[3]).html('<input type="text" id="townUpdateInput" name="town" placeholder="Ville" style="width:' + $(rowObject.children()[3]).width() + '" value=' + updateTrip['town'] + ' required /> ');
	$(rowObject.children()[4]).html('<input type="date" id="beginDateUpdateInput" name="beginDate" style="width:' + $(rowObject.children()[4]).width() + '" value=' + updateTrip['beginDate'] + ' required />');
	$(rowObject.children()[5]).html('<input type="date" id="endDateUpdateInput" name="endDate" style="width:' + $(rowObject.children()[5]).width() + '" value=' + updateTrip['endDate'] + ' required /> ');


	// S'il s'agit de la page admin
	let index = 0;
	let validationFunction = ""
	let url = $(location).attr('href');
	if (url.substring(url.lastIndexOf('/') + 1) === adminPageTripName){
		index = 10;
		validationFunction = "onclick=\"validateUpdateTrip(true)\"";
	}else{
		index = 7;
		validationFunction = "onclick=\"validateUpdateTrip(false)\"";
	}
		

	// On remplace la case action par deux autres boutons (annuler ou valider)
	$(rowObject.children()[index]).children().html("<button id=\"validateUpdateTrip\" " + validationFunction + " > \
													<i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i> \
												</button> \
												<button id=\"cancelUpdateTrip\" onclick=\"cancelUpdateTrip()\"> \
													<i class=\"fa fa-ban fa-lg\" aria-hidden=\"true\"></i> \
												</button>");

});


// Permet de vérifier si les dates saisies sont correctes
function checkDateApplied(beginDate, endDate, isAdmin){
	// On récupère la date d'aujourd'hui à minuit
	let todayDateAtMidnight = new Date();
	todayDateAtMidnight.setHours(0);
	todayDateAtMidnight.setMinutes(0);
	todayDateAtMidnight.setSeconds(0);
	todayDateAtMidnight.setMilliseconds(0);

	if(beginDate <= endDate){
		if(isAdmin || (!isAdmin && todayDateAtMidnight.getTime() <= beginDate)){
			return true;	
		}
		else{
			alert("Erreur saisie dates : la date de début est passée");
			return false;
		}
	}
	else{
		alert("Erreur saisie dates : date de fin avant date de début");
		return false;
	}
}


//#################### Fonction pour l'ajout d'un voyage ####################

// Permet de gérer l'ajout d'un voyage suivant les dates saisies
function validateAddtrip(){
	let beginDate = Date.parse($("#beginDateAddInput").val());
	let endDate = Date.parse($("#endDateAddInput").val());
	return checkDateApplied(beginDate, endDate);		
}

//############################################################################




//################# Fonction pour la mise à jour d'un voyage #################

// Permet de valider la modification d'un voyage
function validateUpdateTrip(isAdmin){
	// On véfie si les champs ne sont pas vides avant l'insertion
	if($('#countryUpdateInput').val() !== "" && $('#townUpdateInput').val() !== "" && $('#beginDateUpdateInput').val() !== "" && $('#endDateUpdateInput').val() !== "" ){

		// On récupère les dates saisie
		let beginDate = Date.parse($("#beginDateUpdateInput").val());
		let endDate = Date.parse($("#endDateUpdateInput").val());

		// Si les dates saisies sont correctes
		if(checkDateApplied(beginDate, endDate, isAdmin) === true){
			// Demande de confirmation pour modification du voyage
			let response = confirm("Vous allez modifier le voyage n°" + rowIndex + "\nVoulez-vous continuer ?");
			// Création de la requête post
			if (response === true) {
				$.ajax({
					url: 'update_trip.php',
				    type: 'POST',			    
				    data : {country: $("#countryUpdateInput").val(),
							town: $("#townUpdateInput").val(), 
							beginDate: $("#beginDateUpdateInput").val(),
							endDate: $("#endDateUpdateInput").val(),
							rowIndex: rowIndex},
				    success: function() {   
				        location.reload();  
				    }
				});	
			}
		}
	}
}


// Permet d'annuler la modification d'un voyage
function cancelUpdateTrip(){
	location.reload();
}
//############################################################################

