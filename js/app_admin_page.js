function createDynamicMap(counts){
	// Create map instance
	let map = am4core.create("worldMap", am4maps.MapChart);

	// Set map definition
	map.geodata = am4geodata_worldLow;

	// Set projection
	map.projection = new am4maps.projections.Miller();

	// Create map polygon series
	let polygonSeries = map.series.push(new am4maps.MapPolygonSeries());

	// Make map load polygon data from GeoJSON
	polygonSeries.useGeodata = true;

	// Configure series
	let polygonTemplate = polygonSeries.mapPolygons.template;
	polygonTemplate.tooltipText = "{name}";
	polygonTemplate.fill = am4core.color("#e8e8e8");

	// Create hover state and set alternative fill color
	let hs = polygonTemplate.states.create("hover");
	hs.properties.fill = am4core.color("#cdcdcd");

	polygonTemplate.properties.value = 0;

	// Excluding Antarctica
	polygonSeries.exclude = ["AQ"];

	if(Object.keys(counts).length === 0 && counts.constructor === Object){
		polygonSeries.data = [];
	}
	else {
		for (let [key, value] of Object.entries(counts)) {
		    polygonSeries.data.push({
		    	id: key,
		    	value: value,
		    	fill: am4core.color("#F05C5C")
		    });
		}
		// Bind "fill" property to "fill" key in data
		polygonTemplate.propertyFields.fill = "fill";

		polygonTemplate.tooltipText = "{name} | Number of trip(s) : {value}";

	}
}

$.ajax({
   url : '../pages/get_all_trips.php',
   type : 'GET', // type of the HTTP request
   success : function(data){
		var obj = jQuery.parseJSON(data);
		var counts = {};
		jQuery.each(obj, function(key,value) {
		  if (!counts.hasOwnProperty(value)) {
		    counts[value] = 1;
		  } else {
		    counts[value]++;
		  }
		});
		createDynamicMap(counts);
   }
});

